/* implementation of parser.h */
#include "parser.h"
#include <stdlib.h>


  char*
get_string(int current_char, int replace_this, char* replacement_string)
{
  char *output;
  if (current_char == replace_this){
    output = replacement_string;
  }
  else{
    output = malloc(sizeof(int) + 1);
    output[0] = current_char;
    output[1] = '\0';
  }
  return output;
}
