#include <stdlib.h>
#include <argp.h>
#include <stdlib.h>

#include "argp_parser.h"

const char *argp_program_version = "0.0.1";
const char *argp_bug_address = "<jha.ayush@mail.com>";



struct argp_arguments args =
{
  DEFAULT_NO_OF_CHARACTERS,
  DEFAULT_INPUT_FILENAME,        /* input file name */
  DEFAULT_OUTPUT_FILENAME,       /* output file name */
  DEFAULT_REPLACE_CHARACTER,     /* default character to replace with */
  DEFAULT_REPLACE_THIS_CHARACTER/* default character to replace */
};

error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct argp_arguments *arguments = state->input;
  switch (key)
  {
    case 's': // replace this character insdead of <tab>
      arguments->replace_this = arg[0];
      break;
    case 'n': //  no. of characters to replace tab with
      arguments->no_of_chars = atoi(arg);
      break;
    case 'i': // read input from this file
      arguments->infilename = arg;
      break;
    case 'o': // send output to this file
      arguments->outfilename = arg;
      break;
    case 'r': /* replace with this character */
      arguments->replacement = arg[0];
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

FILE*
get_input_stream(char *filename)
{
  if (filename == NULL)
    return stdin;
  FILE* in = fopen(filename, "r");
  /* check if stream exists */
  if (in == NULL)
  {
    printf("Error: File '%s' cannot be read!", filename);
    exit(-1);
  }
  return in;
}


FILE*
get_output_stream(char *filename)
{
  if (filename == NULL)
    return stdout;
  FILE *out = fopen(filename, "w");
  if (out == NULL)
  {
    printf("Error: File '%s' cannot be written to!", filename);
    exit(-1);
  }
  return out;
}
