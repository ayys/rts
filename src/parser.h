/* parser header file - parses string */

/* takes a character and processes it to produce the output character */
char* get_string(int current_char, int replace_this, char* replacement_string);
