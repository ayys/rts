/* declarations of argp related variables and functions
 * along with useful macros related to argumetn parsing
 */

#include <argp.h>
#ifndef REPLACE_TABS_WITH_SPACES_HEADER

#define REPLACE_TABS_WITH_SPACES_HEADER
/*
 * this is the default no. of spaces set 
 * */
#define DEFAULT_NO_OF_CHARACTERS 2

#define DEFAULT_INPUT_FILENAME NULL
#define DEFAULT_OUTPUT_FILENAME NULL
#define DEFAULT_REPLACE_CHARACTER ' '
#define DEFAULT_REPLACE_THIS_CHARACTER '\t'

#define  DOCUMENTATION_STRING "RTWS - Replace Tab With Space or other characters \
  is a small utility that converts all the tabs \
in the file to a set of spaces or other characters."

/* This structure is used to communicate with parse_opt. */
struct argp_arguments
{
  int no_of_chars;  /* -n flag */
  char *infilename; /* -i flag */
  char *outfilename;/* -o flag */
  char replacement; /* -r flag. Replace tab with this character */
  char replace_this;/* -s flag. Replace this with replacement */
};

error_t
parse_opt (int key, char *arg, struct argp_state *state);

FILE*
get_input_stream(char *filename);
FILE*
get_output_stream(char *filename);

static struct argp_option options[];
static struct argp argp_struct;
struct argp_arguments args;

#endif
