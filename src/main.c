#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <argp.h>

#include "argp_parser.h"
#include "parser.h"

extern struct argp_arguments args;
static struct
argp_option options[] =
{
  /* number of spaces, default value is DEFAULT_NO_OF_SPACES */
  {"no-of-chars",   'n', "NO_CHAR", 0,
    "Define number of characters to replace tabs with. Default is 2"},
  /* if specified, reads from FILE, not stdin */
  {"input",  'i', "INPUT", 0,
    "Input to FILE instead of standard input"},
  /* if specified, writes to OUTPUT not stdout */
  {"output",  'o', "OUTPUT", 0,
    "Output to OUTPUT instead of standard output"},
  /* give an alternate replacer. default is space */
  {"replace-with", 'r', "CHARACTER", 0,
    "Replace with CHARACTER. Default is ' '"},
  {"replace-this", 's', "CHARACTER", 0,
    "Replace this instead of <tab>. Default is <TAB>"},
  {0}
};

static struct argp argp_struct = {options, parse_opt, 0, DOCUMENTATION_STRING};

/* main function */
int
main(int argc, char* argv[])
{
  FILE *in, *out;
  char *spaces; /* create a string with spaces as per needed */
  int current_character; /* character read from the input stream */

  argp_parse(&argp_struct, argc, argv, 0, 0, &args);

  in = get_input_stream(args.infilename);
  out = get_output_stream(args.outfilename);

  spaces = malloc(args.no_of_chars * sizeof(char));
  memset(spaces, args.replacement, args.no_of_chars);

  while((current_character = fgetc(in)) != EOF){
    fprintf(out, get_string(current_character,
          args.replace_this,
          spaces));
  }

  fclose(in);
  fclose(out);
  return 0;
}
